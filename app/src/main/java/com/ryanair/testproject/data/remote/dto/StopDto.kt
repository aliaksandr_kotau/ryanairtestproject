package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName

data class StopDto(
        @SerializedName("code")
        var code: String? = null
)