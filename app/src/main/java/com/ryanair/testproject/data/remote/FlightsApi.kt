package com.ryanair.testproject.data.remote

import com.ryanair.testproject.core.FLIGHTS_PATH
import com.ryanair.testproject.data.remote.dto.TripsDto
import retrofit2.http.GET
import retrofit2.http.Query

private const val DATE_OUT_PARAM = "dateOut"
private const val ORIGIN_PARAM = "origin"
private const val DESTINATION_PARAM = "destination"
private const val ADT_PARAM = "adt"
private const val TEEN_PARAM = "teen"
private const val CHD_PARAM = "chd"
private const val INF_PARAM = "inf"

interface FlightsApi {

    @GET(FLIGHTS_PATH)
    suspend fun getTrips(@Query(DATE_OUT_PARAM) departureDate: String,
                         @Query(ORIGIN_PARAM) departureStation: String,
                         @Query(DESTINATION_PARAM) arrivalStation: String,
                         @Query(ADT_PARAM) adults: String,
                         @Query(TEEN_PARAM) teens: String,
                         @Query(CHD_PARAM) children: String,
                         @Query(INF_PARAM) infants: String): TripsDto
}