package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.Flight

data class FlightDto(
        @SerializedName("faresLeft")
        var faresLeft: Int? = null,
        @SerializedName("flightKey")
        var flightKey: String? = null,
        @SerializedName("infantsLeft")
        var infantsLeft: Int? = null,
        @SerializedName("regularFare")
        var regularFare: RegularFareDto? = null,
        @SerializedName("operatedBy")
        var operatedBy: String? = null,
        @SerializedName("segments")
        var segments: List<SegmentDto>? = null,
        @SerializedName("flightNumber")
        var flightNumber: String? = null,
        @SerializedName("time")
        var time: List<String>? = null,
        @SerializedName("timeUTC")
        var timeUTC: List<String>? = null,
        @SerializedName("duration")
        var duration: String? = null
)

fun FlightDto.toFlight() = Flight(
        regularFare = regularFare?.toRegularFare(),
        operatedBy = operatedBy,
        segments = segments?.map { it.toSegment() },
        flightNumber = flightNumber,
        time = time,
        timeUTC = timeUTC,
        duration = duration,
)