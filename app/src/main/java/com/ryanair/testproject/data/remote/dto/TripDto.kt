package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.Trip

data class TripDto(
        @SerializedName("origin")
        var origin: String? = null,
        @SerializedName("originName")
        var originName: String? = null,
        @SerializedName("destination")
        var destination: String? = null,
        @SerializedName("destinationName")
        var destinationName: String? = null,
        @SerializedName("routeGroup")
        var routeGroup: String? = null,
        @SerializedName("tripType")
        var tripType: String? = null,
        @SerializedName("upgradeType")
        var upgradeType: String? = null,
        @SerializedName("dates")
        var dates: List<FlightsDateDto>? = null
)

fun TripDto.toTrip() = Trip(
        origin = origin,
        originName = originName,
        destination = destination,
        destinationName = destinationName,
        dates = dates?.map { it.toFlightsDates() }
)