package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.Fare

data class FareDto(
        @SerializedName("type")
        var type: String? = null,
        @SerializedName("amount")
        var amount: Double? = null,
        @SerializedName("count")
        var count: Int? = null,
        @SerializedName("hasDiscount")
        var hasDiscount: Boolean? = null,
        @SerializedName("publishedFare")
        var publishedFare: Double? = null,
        @SerializedName("discountInPercent")
        var discountInPercent: Int? = null,
        @SerializedName("hasPromoDiscount")
        var hasPromoDiscount: Boolean? = null,
        @SerializedName("discountAmount")
        var discountAmount: Double? = null,
        @SerializedName("hasBogof")
        var hasBogof: Boolean? = null
)

fun FareDto.toFare() = Fare(
        type = type,
        amount = amount,
        count = count
)