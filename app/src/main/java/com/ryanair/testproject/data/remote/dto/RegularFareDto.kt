package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.RegularFare

data class RegularFareDto(
        @SerializedName("fareKey")
        var fareKey: String? = null,
        @SerializedName("fareClass")
        var fareClass: String? = null,
        @SerializedName("fares")
        var fares: List<FareDto>? = null
)

fun RegularFareDto.toRegularFare() = RegularFare(
        fares = fares?.map { it.toFare() }
)