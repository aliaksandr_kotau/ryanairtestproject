package com.ryanair.testproject.data.repository

import com.ryanair.testproject.data.remote.FlightsApi
import com.ryanair.testproject.data.remote.StationsApi
import com.ryanair.testproject.data.remote.dto.toStation
import com.ryanair.testproject.data.remote.dto.toTrips
import com.ryanair.testproject.domain.repository.RyanairRepository

class RyanairRepositoryImpl(private val stationsApi: StationsApi,
                            private val flightsApi: FlightsApi) : RyanairRepository {

    override suspend fun getStations() = stationsApi.getStations().stations?.map { it.toStation() } ?: emptyList()
    override suspend fun getTrips(departureDate: String,
                                  departureStation: String,
                                  arrivalStation: String,
                                  adults: String,
                                  teens: String,
                                  children: String,
                                  infants: String) =
        flightsApi.getTrips(departureDate,
                departureStation,
                arrivalStation,
                adults,
                teens,
                children,
                infants).toTrips()
}