package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName

data class MarketDto(
        @SerializedName("code")
        var code: String? = null,
        @SerializedName("group")
        var group: Any? = null,
        @SerializedName("stops")
        var stops: List<StopDto>? = null,
        @SerializedName("onlyConnecting")
        var onlyConnecting: Boolean? = null,
        @SerializedName("pendingApproval")
        var pendingApproval: Boolean? = null,
        @SerializedName("onlyPrintedBoardingPass")
        var onlyPrintedBoardingPass: Boolean? = null
)