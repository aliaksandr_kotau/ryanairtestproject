package com.ryanair.testproject.data.remote

import com.ryanair.testproject.core.STATIONS_PATH
import com.ryanair.testproject.data.remote.dto.StationsDto
import retrofit2.http.GET

interface StationsApi {

    @GET(STATIONS_PATH)
    suspend fun getStations(): StationsDto
}