package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.Station

data class StationDto(
        @SerializedName("code")
        var code: String? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("alternateName")
        var alternateName: Any? = null,
        @SerializedName("alias")
        var alias: List<String>? = null,
        @SerializedName("countryCode")
        var countryCode: String? = null,
        @SerializedName("countryName")
        var countryName: String? = null,
        @SerializedName("countryAlias")
        var countryAlias: Any? = null,
        @SerializedName("countryGroupCode")
        var countryGroupCode: String? = null,
        @SerializedName("countryGroupName")
        var countryGroupName: String? = null,
        @SerializedName("timeZoneCode")
        var timeZoneCode: String? = null,
        @SerializedName("latitude")
        var latitude: String? = null,
        @SerializedName("longitude")
        var longitude: String? = null,
        @SerializedName("mobileBoardingPass")
        var mobileBoardingPass: Boolean? = null,
        @SerializedName("markets")
        var markets: List<MarketDto>? = null,
        @SerializedName("notices")
        var notices: Any? = null,
        @SerializedName("tripCardImageUrl")
        var tripCardImageUrl: String? = null,
        @SerializedName("airportShopping")
        var airportShopping: Boolean? = null
)

fun StationDto.toStation() = Station(
        code = code,
        name = name,
        countryCode = countryCode,
        countryName = countryName
)