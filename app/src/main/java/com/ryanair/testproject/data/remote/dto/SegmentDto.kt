package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.Segment

data class SegmentDto(
        @SerializedName("segmentNr")
        var segmentNr: Int? = null,
        @SerializedName("origin")
        var origin: String? = null,
        @SerializedName("destination")
        var destination: String? = null,
        @SerializedName("flightNumber")
        var flightNumber: String? = null,
        @SerializedName("time")
        var time: List<String>? = null,
        @SerializedName("timeUTC")
        var timeUTC: List<String>? = null,
        @SerializedName("duration")
        var duration: String? = null
)

fun SegmentDto.toSegment() = Segment(
        origin = origin,
        destination = destination,
        flightNumber = flightNumber,
        time = time,
        timeUTC = timeUTC,
        duration = duration
)