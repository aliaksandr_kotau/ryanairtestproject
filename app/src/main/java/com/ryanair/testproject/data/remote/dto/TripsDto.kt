package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.Trips

data class TripsDto(
        @SerializedName("termsOfUse")
        var termsOfUse: String? = null,
        @SerializedName("currency")
        var currency: String? = null,
        @SerializedName("currPrecision")
        var currPrecision: Int? = null,
        @SerializedName("routeGroup")
        var routeGroup: String? = null,
        @SerializedName("tripType")
        var tripType: String? = null,
        @SerializedName("upgradeType")
        var upgradeType: String? = null,
        @SerializedName("trips")
        var trips: List<TripDto>? = null,
        @SerializedName("serverTimeUTC")
        var serverTimeUTC: String? = null
)

fun TripsDto.toTrips() = Trips(
        currency = currency,
        trips = trips?.map { it.toTrip() }
)