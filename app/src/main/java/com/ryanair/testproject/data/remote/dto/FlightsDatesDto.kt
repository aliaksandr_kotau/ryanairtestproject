package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.ryanair.testproject.domain.model.FlightsDates

data class FlightsDateDto(
        @SerializedName("dateOut")
        var dateOut: String? = null,
        @SerializedName("flights")
        var flights: List<FlightDto>? = null
)

fun FlightsDateDto.toFlightsDates() = FlightsDates(
        flights = flights?.map { it.toFlight() }
)