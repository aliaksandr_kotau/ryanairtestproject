package com.ryanair.testproject.data.remote.dto


import com.google.gson.annotations.SerializedName

data class StationsDto(
        @SerializedName("stations")
        var stations: List<StationDto>? = null
)