package com.ryanair.testproject.domain.repository

import com.ryanair.testproject.domain.model.Station
import com.ryanair.testproject.domain.model.Trips

interface RyanairRepository {

    suspend fun getStations(): List<Station>
    suspend fun getTrips(departureDate: String,
                         departureStation: String,
                         arrivalStation: String,
                         adults: String,
                         teens: String,
                         children: String,
                         infants: String): Trips
}