package com.ryanair.testproject.domain.usecase

import com.ryanair.testproject.R
import com.ryanair.testproject.core.Resource
import com.ryanair.testproject.core.Utils
import com.ryanair.testproject.domain.model.Station
import com.ryanair.testproject.domain.repository.RyanairRepository
import com.ryanair.testproject.presentation.stations.BaseStationListItem
import com.ryanair.testproject.presentation.stations.CountryListItem
import com.ryanair.testproject.presentation.stations.StationListItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetStationsListItemsUseCase @Inject constructor(private val ryanairRepository: RyanairRepository,
                                                      private val utils: Utils) {

    operator fun invoke(): Flow<Resource<List<BaseStationListItem>>> = flow {
        try {
            emit(Resource.Loading())

            val stations = ryanairRepository.getStations()
            val stationsByCountry = getStationsGroupedByCountry(stations)
            val stationsListItems = getStationsListItems(stationsByCountry)

            emit(Resource.Success(stationsListItems))
        } catch (exception: Exception) {
            val errorMessage = when (exception) {
                is HttpException -> {
                    if (!exception.message().isNullOrBlank())
                        exception.message()
                    else utils.getString(R.string.error_unknown)
                }
                is IOException -> utils.getString(R.string.error_network)
                else -> utils.getString(R.string.error_unknown)
            }
            emit(Resource.Error(errorMessage))
        }
    }

    private fun getStationsGroupedByCountry(stations: List<Station>): Map<String, List<Station>> {
        val stationsByCountry = mutableMapOf<String, MutableList<Station>>()

        stations.forEach {
            val country = it.countryName ?: ""

            if (stationsByCountry[country] == null) {
                stationsByCountry[country] = mutableListOf()
            }

            stationsByCountry[country]?.add(it)
        }

        return stationsByCountry.toSortedMap().apply {
            keys.sorted()
        }
    }

    private fun getStationsListItems(stationsByCountry: Map<String, List<Station>>): List<BaseStationListItem> {
        val listItems = mutableListOf<BaseStationListItem>()

        stationsByCountry.forEach { entry ->
            listItems.add(CountryListItem(entry.key))
            listItems.addAll(entry.value.map { StationListItem(it) })
        }

        return listItems
    }
}