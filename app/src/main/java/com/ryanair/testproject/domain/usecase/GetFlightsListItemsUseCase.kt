package com.ryanair.testproject.domain.usecase

import com.ryanair.testproject.R
import com.ryanair.testproject.core.Resource
import com.ryanair.testproject.core.Utils
import com.ryanair.testproject.domain.model.Trips
import com.ryanair.testproject.domain.repository.RyanairRepository
import com.ryanair.testproject.presentation.flights.FlightListItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetFlightsListItemsUseCase @Inject constructor(private val ryanairRepository: RyanairRepository,
                                                     private val utils: Utils) {

    operator fun invoke(departureDate: String,
                        departureStation: String,
                        arrivalStation: String,
                        adults: String,
                        teens: String,
                        children: String,
                        infants: String): Flow<Resource<List<FlightListItem>>> = flow {
        try {
            emit(Resource.Loading())

            val trips = ryanairRepository.getTrips(departureDate,
                    departureStation,
                    arrivalStation,
                    adults,
                    teens,
                    children,
                    infants)
            val flightsListItems = getFlightsListItems(trips)
            if (flightsListItems.isEmpty()) {
                emit(Resource.Error(utils.getString(R.string.error_no_flights_available)))
            } else {
                emit(Resource.Success(flightsListItems))
            }
        } catch (exception: Exception) {
            val errorMessage = when (exception) {
                is HttpException -> {
                    if (!exception.message().isNullOrBlank())
                        exception.message()
                    else utils.getString(R.string.error_unknown)
                }
                is IOException -> utils.getString(R.string.error_network)
                else -> utils.getString(R.string.error_unknown)
            }
            emit(Resource.Error(errorMessage))
        }
    }

    private fun getFlightsListItems(trips: Trips) =
        trips.trips?.firstOrNull()?.dates?.firstOrNull()?.flights?.map {
            FlightListItem(trips.currency, it)
        } ?: emptyList()
}