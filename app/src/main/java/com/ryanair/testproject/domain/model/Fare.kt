package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Fare(
        var type: String? = null,
        var amount: Double? = null,
        var count: Int? = null,
) : Parcelable