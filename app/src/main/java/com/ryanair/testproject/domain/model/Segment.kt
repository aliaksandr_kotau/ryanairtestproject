package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Segment(
        var origin: String? = null,
        var destination: String? = null,
        var flightNumber: String? = null,
        var time: List<String>? = null,
        var timeUTC: List<String>? = null,
        var duration: String? = null
) : Parcelable