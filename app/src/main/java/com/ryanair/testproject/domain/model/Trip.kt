package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Trip(
        var origin: String? = null,
        var originName: String? = null,
        var destination: String? = null,
        var destinationName: String? = null,
        var dates: List<FlightsDates>? = null
) : Parcelable