package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Station(
        var code: String? = null,
        var name: String? = null,
        var countryCode: String? = null,
        var countryName: String? = null,
): Parcelable