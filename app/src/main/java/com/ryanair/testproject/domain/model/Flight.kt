package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Flight(
        var regularFare: RegularFare? = null,
        var operatedBy: String? = null,
        var segments: List<Segment>? = null,
        var flightNumber: String? = null,
        var time: List<String>? = null,
        var timeUTC: List<String>? = null,
        var duration: String? = null
) : Parcelable