package com.ryanair.testproject.domain.model


data class Trips(
        var currency: String? = null,
        var trips: List<Trip>? = null,
)