package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class FlightsDates(
        var flights: List<Flight>? = null
) : Parcelable