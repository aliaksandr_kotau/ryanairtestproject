package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RegularFare(
        var fares: List<Fare>? = null
) : Parcelable