package com.ryanair.testproject.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PassengersData(
        var adultCount: Int = 1,
        var teenCount: Int = 0,
        var childrenCount: Int = 0,
        var infantCount: Int = 0
) : Parcelable