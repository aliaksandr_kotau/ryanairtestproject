package com.ryanair.testproject.presentation.flights

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ryanair.testproject.R
import com.ryanair.testproject.databinding.FragmentFlightsBinding
import com.ryanair.testproject.domain.model.Flight
import com.ryanair.testproject.domain.model.PassengersData
import com.ryanair.testproject.domain.model.Station
import dagger.hilt.android.AndroidEntryPoint

private const val DEPARTURE_DATE_ARG = "departure_date_arg"
private const val DEPARTURE_STATION_ARG = "departure_station_arg"
private const val ARRIVAL_STATION_ARG = "arrival_station_arg"
private const val PASSENGERS_DATA_ARG = "passengers_data_arg"

interface FlightsNavigationListener {
    fun onFlightSelected(currency: String, flight: Flight)
}

@AndroidEntryPoint
class FlightsFragment : Fragment(R.layout.fragment_flights), FlightClickListener {

    companion object {
        fun newInstance(departureDate: Long?,
                        departureStation: Station?,
                        arrivalStation: Station?,
                        passengerData: PassengersData?) = FlightsFragment().apply {
            arguments = Bundle().apply {
                putLong(DEPARTURE_DATE_ARG, departureDate ?: 0)
                putParcelable(DEPARTURE_STATION_ARG, departureStation)
                putParcelable(ARRIVAL_STATION_ARG, arrivalStation)
                putParcelable(PASSENGERS_DATA_ARG, passengerData)
            }
        }
    }

    private val flightsViewModel by viewModels<FlightsViewModel>()
    private val adapter = FlightsAdapter(this)
    private lateinit var flightsNavigationListener: FlightsNavigationListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        flightsNavigationListener = context as FlightsNavigationListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentFlightsBinding.bind(view)

        (activity as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        flightsViewModel.title.observe(viewLifecycleOwner) { title ->
            activity?.title = title
        }

        flightsViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.progress.isVisible = isLoading
        }

        flightsViewModel.data.observe(viewLifecycleOwner) { data ->
            adapter.submitList(data)
        }

        flightsViewModel.errorMessage.observe(viewLifecycleOwner) { errorMessage ->
            MaterialAlertDialogBuilder(requireContext())
                    .setMessage(errorMessage)
                    .setOnDismissListener {
                        activity?.onBackPressed()
                    }
                    .show()
        }

        binding.flights.adapter = adapter

        flightsViewModel.getFlightsData(arguments?.getLong(DEPARTURE_DATE_ARG),
                arguments?.getParcelable(DEPARTURE_STATION_ARG),
                arguments?.getParcelable(ARRIVAL_STATION_ARG),
                arguments?.getParcelable(PASSENGERS_DATA_ARG))
    }

    override fun onFlightClick(currency: String, flight: Flight) {
        flightsNavigationListener.onFlightSelected(currency, flight)
    }
}