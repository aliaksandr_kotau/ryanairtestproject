package com.ryanair.testproject.presentation.passengers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ryanair.testproject.domain.model.PassengersData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PassengersViewModel @Inject constructor() : ViewModel() {

    private val _passengersData = MutableLiveData<PassengersData>()
    val passengersData = _passengersData as LiveData<PassengersData>

    fun setInitialPassengerLiveData(passengersData: PassengersData) {
        _passengersData.value = passengersData
    }

    fun onRemoveAdultClick() {
        val passengerData = _passengersData.value
        passengerData?.adultCount?.let {
            if (it > 1) {
                _passengersData.value = passengerData.apply {
                    adultCount--
                }
            }
        }
    }

    fun onAddAdultClick() {
        val passengerData = _passengersData.value
        passengerData?.adultCount?.let {
            _passengersData.value = passengerData.apply {
                adultCount++
            }
        }
    }

    fun onRemoveTeenClick() {
        val passengerData = _passengersData.value
        passengerData?.teenCount?.let {
            if (it > 0) {
                _passengersData.value = passengerData.apply {
                    teenCount--
                }
            }
        }
    }

    fun onAddTeenClick() {
        val passengerData = _passengersData.value
        passengerData?.teenCount?.let {
            _passengersData.value = passengerData.apply {
                teenCount++
            }
        }
    }

    fun onRemoveChildClick() {
        val passengerData = _passengersData.value
        passengerData?.childrenCount?.let {
            if (it > 0) {
                _passengersData.value = passengerData.apply {
                    childrenCount--
                }
            }
        }
    }

    fun onAddChildClick() {
        val passengerData = _passengersData.value
        passengerData?.childrenCount?.let {
            _passengersData.value = passengerData.apply {
                childrenCount++
            }
        }
    }

    fun onRemoveInfantClick() {
        val passengerData = _passengersData.value
        passengerData?.infantCount?.let {
            if (it > 0) {
                _passengersData.value = passengerData.apply {
                    infantCount--
                }
            }
        }
    }

    fun onAddInfantClick() {
        val passengerData = _passengersData.value
        passengerData?.infantCount?.let {
            _passengersData.value = passengerData.apply {
                infantCount++
            }
        }
    }
}