package com.ryanair.testproject.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ryanair.testproject.R
import com.ryanair.testproject.core.SingleLiveEvent
import com.ryanair.testproject.core.Utils
import com.ryanair.testproject.domain.model.PassengersData
import com.ryanair.testproject.domain.model.Station
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val utils: Utils) : ViewModel() {

    private val _isFlightsSearchAvailable = SingleLiveEvent<Boolean>()
    val isFlightsSearchAvailable = _isFlightsSearchAvailable as LiveData<Boolean>

    fun getDepartureDate(departureDateInMillis: Long?) = departureDateInMillis?.let {
        val dateFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault())
        dateFormat.format(Date(it))
    } ?: ""

    fun getPassengers(passengersData: PassengersData?) =
        passengersData?.let {
            val passengersBuilder = StringBuilder()

            passengersBuilder.append(utils.getQuantityString(R.plurals.adults,
                    it.adultCount,
                    it.adultCount))

            if (it.teenCount > 0) {
                passengersBuilder.append(", ${
                    utils.getQuantityString(R.plurals.teens,
                            it.teenCount,
                            it.teenCount)
                }")
            }

            if (it.childrenCount > 0) {
                passengersBuilder.append(", ${
                    utils.getQuantityString(R.plurals.children,
                            it.childrenCount,
                            it.childrenCount)
                }")
            }

            if (it.infantCount > 0) {
                passengersBuilder.append(", ${
                    utils.getQuantityString(R.plurals.infants,
                            it.infantCount,
                            it.infantCount)
                }")
            }

            passengersBuilder.toString()
        } ?: ""

    fun onSearchFlightsClick(departureDate: Long?,
                             departureStation: Station?,
                             arrivalStation: Station?,
                             passengerData: PassengersData?) {
        _isFlightsSearchAvailable.value = departureDate != null
                && departureStation != null
                && arrivalStation != null
                && passengerData != null
    }
}