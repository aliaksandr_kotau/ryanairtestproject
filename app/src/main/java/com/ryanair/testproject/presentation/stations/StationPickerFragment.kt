package com.ryanair.testproject.presentation.stations

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ryanair.testproject.R
import com.ryanair.testproject.databinding.FragmentStationsPickerBinding
import com.ryanair.testproject.domain.model.Station
import dagger.hilt.android.AndroidEntryPoint

private const val IS_DEPARTURE_ARG = "is_departure_arg"

interface StationPickerNavigationListener {
    fun onStationSelected()
}

@AndroidEntryPoint
class StationPickerFragment : Fragment(R.layout.fragment_stations_picker), StationClickListener {

    companion object {
        internal const val DEPARTURE_REQUEST_KEY = "departure_request_key"
        internal const val ARRIVAL_REQUEST_KEY = "arrival_request_key"
        internal const val STATION_ARG = "station_arg"

        fun newInstance(isDeparture: Boolean) = StationPickerFragment().apply {
            arguments = bundleOf(IS_DEPARTURE_ARG to isDeparture)
        }
    }

    private val stationPickerViewModel by viewModels<StationPickerViewModel>()
    private lateinit var stationPickerNavigationListener: StationPickerNavigationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        stationPickerNavigationListener = context as StationPickerNavigationListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentStationsPickerBinding.bind(view)

        (activity as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity?.title = getString(R.string.stations_title)

        val adapter = StationPickerAdapter(this)
        binding.stations.adapter = adapter

        stationPickerViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.progress.isVisible = isLoading
        }

        stationPickerViewModel.data.observe(viewLifecycleOwner) { data ->
            adapter.submitList(data)
        }

        stationPickerViewModel.errorMessage.observe(viewLifecycleOwner) { errorMessage ->
            MaterialAlertDialogBuilder(requireContext())
                    .setMessage(errorMessage)
                    .setOnDismissListener {
                        activity?.onBackPressed()
                    }
                    .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_stations, menu)

        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as? SearchManager
        val searchMenuItem = menu.findItem(R.id.search)
        val searchView = searchMenuItem?.actionView as? SearchView
        searchView?.setIconifiedByDefault(false)
        searchView?.setSearchableInfo(searchManager?.getSearchableInfo(activity?.componentName))
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                stationPickerViewModel.applyQuery(newText)
                return true
            }
        })
    }

    override fun onStationClick(station: Station) {
        val isDeparture = arguments?.getBoolean(IS_DEPARTURE_ARG) ?: true

        val requestKey = if (isDeparture)
            DEPARTURE_REQUEST_KEY
        else ARRIVAL_REQUEST_KEY

        val stationBundle = Bundle().apply {
            putParcelable(STATION_ARG, station)
        }

        setFragmentResult(requestKey, stationBundle)
        stationPickerNavigationListener.onStationSelected()
    }
}