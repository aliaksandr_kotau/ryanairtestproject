package com.ryanair.testproject.presentation.stations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.ryanair.testproject.R
import com.ryanair.testproject.databinding.ListItemCountryBinding
import com.ryanair.testproject.databinding.ListItemStationBinding
import com.ryanair.testproject.domain.model.Station

interface StationClickListener {
    fun onStationClick(station: Station)
}

class StationPickerAdapter(private val listener: StationClickListener) : ListAdapter<BaseStationListItem, BaseStationPickerViewHolder>(StationPickerItemCallback()) {

    @LayoutRes
    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is CountryListItem -> R.layout.list_item_country
        is StationListItem -> R.layout.list_item_station
    }

    override fun onCreateViewHolder(parent: ViewGroup, @LayoutRes viewType: Int): BaseStationPickerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            R.layout.list_item_country -> CountryViewHolder(ListItemCountryBinding.inflate(inflater, parent, false))
            R.layout.list_item_station -> StationViewHolder(ListItemStationBinding.inflate(inflater, parent, false), listener)
            else -> throw IllegalArgumentException("Unknown viewType: $viewType")
        }
    }

    override fun onBindViewHolder(holder: BaseStationPickerViewHolder, position: Int) {
        val listItem = getItem(position)
        when (holder) {
            is CountryViewHolder -> holder.bind(listItem as CountryListItem)
            is StationViewHolder -> holder.bind(listItem as StationListItem)
        }
    }
}

sealed class BaseStationPickerViewHolder(viewBinding: ViewBinding): RecyclerView.ViewHolder(viewBinding.root)
class CountryViewHolder(private val listItemCountryBinding: ListItemCountryBinding) : BaseStationPickerViewHolder(listItemCountryBinding) {

    fun bind(countryListItem: CountryListItem) {
        listItemCountryBinding.country.text = countryListItem.name
    }
}

class StationViewHolder(private val listItemStationBinding: ListItemStationBinding,
                        private val listener: StationClickListener) : BaseStationPickerViewHolder(listItemStationBinding) {

    fun bind(stationListItem: StationListItem) {
        listItemStationBinding.airport.text = stationListItem.station.name
        listItemStationBinding.airport.setOnClickListener {
            listener.onStationClick(stationListItem.station)
        }
    }
}

sealed class BaseStationListItem
data class CountryListItem(val name: String) : BaseStationListItem()
data class StationListItem(val station: Station) : BaseStationListItem()

class StationPickerItemCallback : DiffUtil.ItemCallback<BaseStationListItem>() {

    override fun areItemsTheSame(oldItem: BaseStationListItem,
                                 newItem: BaseStationListItem) = true

    override fun areContentsTheSame(oldItem: BaseStationListItem,
                                    newItem: BaseStationListItem) = true
}