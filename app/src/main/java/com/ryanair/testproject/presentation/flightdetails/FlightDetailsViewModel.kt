package com.ryanair.testproject.presentation.flightdetails

import androidx.lifecycle.ViewModel
import com.ryanair.testproject.domain.model.Flight
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FlightDetailsViewModel @Inject constructor() : ViewModel() {

    fun getTitle(flight: Flight?) =
        "${flight?.segments?.firstOrNull()?.origin ?: ""} -> ${flight?.segments?.lastOrNull()?.destination ?: ""}"

    fun getPrice(currency: String?, flight: Flight?): String {
        val totalFare = flight?.regularFare?.fares?.mapNotNull { it.amount?.times(it.count ?: 1) }?.sumOf { it }
        return "$totalFare $currency"
    }
}