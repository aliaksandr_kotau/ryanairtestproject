package com.ryanair.testproject.presentation.passengers

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.ryanair.testproject.R
import com.ryanair.testproject.databinding.FragmentPassengersBinding
import com.ryanair.testproject.domain.model.PassengersData
import dagger.hilt.android.AndroidEntryPoint

interface PassengersNavigationListener {
    fun onPassengersDataSelected()
}

@AndroidEntryPoint
class PassengersFragment : Fragment(R.layout.fragment_passengers) {

    companion object {
        internal const val PASSENGERS_REQUEST_KEY = "passengers_request_key"
        internal const val PASSENGERS_DATA_ARG = "passengers_data_arg"

        fun newInstance(passengerData: PassengersData) = PassengersFragment().apply {
            arguments = Bundle().apply {
                putParcelable(PASSENGERS_DATA_ARG, passengerData)
            }
        }
    }

    private val passengersViewModel by viewModels<PassengersViewModel>()
    private lateinit var binding: FragmentPassengersBinding
    private lateinit var passengersNavigationListener: PassengersNavigationListener
    private var passengersData: PassengersData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        passengersNavigationListener = context as PassengersNavigationListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentPassengersBinding.bind(view)

        (activity as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity?.title = getString(R.string.passengers_title)

        passengersData = arguments?.getParcelable(PASSENGERS_DATA_ARG)
        passengersData?.let { data ->
            passengersViewModel.setInitialPassengerLiveData(data)

            binding.adultsAmount.text = data.adultCount.toString()
            binding.teensAmount.text = data.teenCount.toString()
            binding.childrenAmount.text = data.childrenCount.toString()
            binding.infantAmount.text = data.infantCount.toString()
        }

        passengersViewModel.passengersData.observe(viewLifecycleOwner) { passengersData ->
            this.passengersData = passengersData

            binding.adultsAmount.text = passengersData.adultCount.toString()
            binding.teensAmount.text = passengersData.teenCount.toString()
            binding.childrenAmount.text = passengersData.childrenCount.toString()
            binding.infantAmount.text = passengersData.infantCount.toString()
        }

        addListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_passengers, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        R.id.save -> {
            setFragmentResult(PASSENGERS_REQUEST_KEY, Bundle().apply {
                putParcelable(PASSENGERS_DATA_ARG, passengersData)
            })
            passengersNavigationListener.onPassengersDataSelected()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun addListeners() {
        binding.removeAdult.setOnClickListener {
            passengersViewModel.onRemoveAdultClick()
        }

        binding.addAdult.setOnClickListener {
            passengersViewModel.onAddAdultClick()
        }

        binding.removeTeen.setOnClickListener {
            passengersViewModel.onRemoveTeenClick()
        }

        binding.addTeen.setOnClickListener {
            passengersViewModel.onAddTeenClick()
        }

        binding.removeChild.setOnClickListener {
            passengersViewModel.onRemoveChildClick()
        }

        binding.addChild.setOnClickListener {
            passengersViewModel.onAddChildClick()
        }

        binding.removeInfant.setOnClickListener {
            passengersViewModel.onRemoveInfantClick()
        }

        binding.addInfant.setOnClickListener {
            passengersViewModel.onAddInfantClick()
        }
    }
}