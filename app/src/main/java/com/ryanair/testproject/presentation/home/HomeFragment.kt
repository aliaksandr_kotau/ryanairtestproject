package com.ryanair.testproject.presentation.home

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ryanair.testproject.R
import com.ryanair.testproject.databinding.FragmentHomeBinding
import com.ryanair.testproject.domain.model.PassengersData
import com.ryanair.testproject.domain.model.Station
import com.ryanair.testproject.presentation.passengers.PassengersFragment.Companion.PASSENGERS_DATA_ARG
import com.ryanair.testproject.presentation.passengers.PassengersFragment.Companion.PASSENGERS_REQUEST_KEY
import com.ryanair.testproject.presentation.stations.StationPickerFragment.Companion.ARRIVAL_REQUEST_KEY
import com.ryanair.testproject.presentation.stations.StationPickerFragment.Companion.DEPARTURE_REQUEST_KEY
import com.ryanair.testproject.presentation.stations.StationPickerFragment.Companion.STATION_ARG
import dagger.hilt.android.AndroidEntryPoint

interface HomeNavigationListener {
    fun openStationsPicker(isDeparture: Boolean)
    fun openPassengersPicker(passengersData: PassengersData)
    fun openFlightsScreen(departureDate: Long?,
                          departureStation: Station?,
                          arrivalStation: Station?,
                          passengerData: PassengersData?)
}

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home) {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private val homeViewModel by viewModels<HomeViewModel>()

    private lateinit var homeNavigationListener: HomeNavigationListener
    private lateinit var binding: FragmentHomeBinding

    private var departureDate: Long? = null
    private var departureStation: Station? = null
    private var arrivalStation: Station? = null
    private var passengersData: PassengersData? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeNavigationListener = context as HomeNavigationListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentHomeBinding.bind(view)

        (activity as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)
        activity?.title = getString(R.string.app_name)

        homeViewModel.isFlightsSearchAvailable.observe(viewLifecycleOwner) { isFlightsSearchAvailable ->
            if (isFlightsSearchAvailable) {
                homeNavigationListener.openFlightsScreen(departureDate,
                        departureStation,
                        arrivalStation,
                        passengersData)
            } else {
                MaterialAlertDialogBuilder(requireContext())
                        .setMessage(getString(R.string.error_missed_flight_info))
                        .show()
            }
        }

        addResultListeners()
        addListeners()
    }

    private fun addResultListeners() {
        setFragmentResultListener(DEPARTURE_REQUEST_KEY) { _, bundle ->
            val station = bundle.getParcelable<Station>(STATION_ARG)

            departureStation = station
            binding.from.setText(station?.name)
        }

        setFragmentResultListener(ARRIVAL_REQUEST_KEY) { _, bundle ->
            val station = bundle.getParcelable<Station>(STATION_ARG)

            arrivalStation = station
            binding.to.setText(station?.name)
        }

        setFragmentResultListener(PASSENGERS_REQUEST_KEY) { _, bundle ->
            val passengersData = bundle.getParcelable<PassengersData>(PASSENGERS_DATA_ARG)

            this.passengersData = passengersData
            binding.passengers.setText(homeViewModel.getPassengers(passengersData))
        }
    }

    private fun addListeners() {
        binding.from.setOnClickListener {
            homeNavigationListener.openStationsPicker(true)
        }
        binding.to.setOnClickListener {
            homeNavigationListener.openStationsPicker(false)
        }
        binding.departureDate.setOnClickListener {
            MaterialDatePicker.Builder.datePicker()
                    .setTitleText(getString(R.string.date_picker_title))
                    .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                    .build()
                    .apply {
                        addOnPositiveButtonClickListener {
                            departureDate = it
                            binding.departureDate.setText(homeViewModel.getDepartureDate(it))

                        }
                    }.show(childFragmentManager, null)
        }
        binding.passengers.setOnClickListener {
            homeNavigationListener.openPassengersPicker(passengersData ?: PassengersData())
        }
        binding.search.setOnClickListener {
            homeViewModel.onSearchFlightsClick(departureDate,
                    departureStation,
                    arrivalStation,
                    passengersData)
        }
    }
}