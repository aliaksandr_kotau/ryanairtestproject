package com.ryanair.testproject.presentation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ryanair.testproject.R
import com.ryanair.testproject.domain.model.Flight
import com.ryanair.testproject.domain.model.PassengersData
import com.ryanair.testproject.domain.model.Station
import com.ryanair.testproject.presentation.flightdetails.FlightDetailsFragment
import com.ryanair.testproject.presentation.flights.FlightsFragment
import com.ryanair.testproject.presentation.flights.FlightsNavigationListener
import com.ryanair.testproject.presentation.home.HomeFragment
import com.ryanair.testproject.presentation.home.HomeNavigationListener
import com.ryanair.testproject.presentation.passengers.PassengersFragment
import com.ryanair.testproject.presentation.passengers.PassengersNavigationListener
import com.ryanair.testproject.presentation.stations.StationPickerFragment
import com.ryanair.testproject.presentation.stations.StationPickerNavigationListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(),
        HomeNavigationListener,
        StationPickerNavigationListener,
        PassengersNavigationListener,
        FlightsNavigationListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        changeFragment(HomeFragment.newInstance())
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            super.onBackPressed()
        }
    }

    override fun openStationsPicker(isDeparture: Boolean) {
        changeFragment(StationPickerFragment.newInstance(isDeparture), true)
    }

    override fun openPassengersPicker(passengersData: PassengersData) {
        changeFragment(PassengersFragment.newInstance(passengersData), true)
    }

    override fun openFlightsScreen(departureDate: Long?,
                                   departureStation: Station?,
                                   arrivalStation: Station?,
                                   passengerData: PassengersData?) {
        changeFragment(FlightsFragment.newInstance(departureDate, departureStation, arrivalStation, passengerData), true)
    }

    override fun onStationSelected() {
        supportFragmentManager.popBackStackImmediate()
    }

    override fun onPassengersDataSelected() {
        supportFragmentManager.popBackStackImmediate()
    }

    override fun onFlightSelected(currency: String, flight: Flight) {
        changeFragment(FlightDetailsFragment.newInstance(currency, flight), true)
    }

    private fun changeFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        supportFragmentManager.beginTransaction()
                .apply {
                    if (addToBackStack) {
                        addToBackStack(null)
                    }
                }
                .replace(R.id.fragmentsContainer, fragment)
                .commit()
    }
}