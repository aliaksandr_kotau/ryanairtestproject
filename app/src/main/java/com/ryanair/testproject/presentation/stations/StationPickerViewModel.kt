package com.ryanair.testproject.presentation.stations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ryanair.testproject.core.Resource
import com.ryanair.testproject.domain.usecase.GetStationsListItemsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class StationPickerViewModel @Inject constructor(private val getStationsListItemsUseCase: GetStationsListItemsUseCase) : ViewModel() {

    private var original: List<BaseStationListItem>? = null

    private val _data = MutableLiveData<List<BaseStationListItem>>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _errorMessage = MutableLiveData<String>()

    val data = _data as LiveData<List<BaseStationListItem>>
    val isLoading = _isLoading as LiveData<Boolean>
    val errorMessage = _errorMessage as LiveData<String>

    init {
        getStations()
    }

    fun applyQuery(query: String?) {
        original?.let { original ->
            val filtered = if (!query.isNullOrBlank()) {
                original.filter { listItem ->
                    when(listItem) {
                        is CountryListItem -> {
                            listItem.name.contains(query, true)
                        }
                        is StationListItem -> {
                            listItem.station.name?.contains(query, true) ?: false
                        }
                    }
                }
            } else original

            _data.value = filtered
        }
    }

    private fun getStations() {
        getStationsListItemsUseCase().onEach { resource ->
            when(resource) {
                is Resource.Loading -> {
                    _isLoading.value = true
                }
                is Resource.Success -> {
                    _isLoading.value = false

                    resource.data?.let {
                        original = it
                        _data.value = it
                    }
                }
                is Resource.Error -> {
                    _isLoading.value = false
                    resource.errorMessage?.let {
                        _errorMessage.value = it
                    }
                }
            }
        }.launchIn(viewModelScope)
    }
}