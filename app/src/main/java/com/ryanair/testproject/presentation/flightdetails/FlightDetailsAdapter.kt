package com.ryanair.testproject.presentation.flightdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ryanair.testproject.databinding.ListItemFlightSegmentBinding
import com.ryanair.testproject.domain.model.Segment
import java.text.SimpleDateFormat
import java.util.*

class FlightDetailsAdapter : ListAdapter<Segment, FlightDetailsViewHolder>(FlightDetailsItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightDetailsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FlightDetailsViewHolder(ListItemFlightSegmentBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: FlightDetailsViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}

class FlightDetailsViewHolder(private val binding: ListItemFlightSegmentBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(segment: Segment) {
       binding.flightNumber.text = segment.flightNumber

        val remoteDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
        val timeDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

        val remoteDepartureDateString = segment.time?.firstOrNull()
        val remoteArrivalDateString = segment.time?.lastOrNull()

        if (!remoteArrivalDateString.isNullOrEmpty() && !remoteDepartureDateString.isNullOrEmpty()) {
            val remoteDepartureDate = remoteDateFormat.parse(remoteDepartureDateString) ?: Date()
            binding.departureTime.text = timeDateFormat.format(remoteDepartureDate)

            val remoteArrivalDate = remoteDateFormat.parse(remoteArrivalDateString) ?: Date()
            binding.arrivalTime.text = timeDateFormat.format(remoteArrivalDate)

            val duration = "Duration: ${timeDateFormat.format(Date(remoteArrivalDate.time - remoteDepartureDate.time))}"
            binding.duration.text = duration
        }

        binding.departureStation.text = segment.origin
        binding.arrivalStation.text = segment.destination
    }
}

class FlightDetailsItemCallback : DiffUtil.ItemCallback<Segment>() {
    override fun areItemsTheSame(oldItem: Segment, newItem: Segment) = true
    override fun areContentsTheSame(oldItem: Segment, newItem: Segment) = true
}