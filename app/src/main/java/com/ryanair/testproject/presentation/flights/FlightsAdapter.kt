package com.ryanair.testproject.presentation.flights

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ryanair.testproject.databinding.ListItemFlightBinding
import com.ryanair.testproject.domain.model.Flight
import java.text.SimpleDateFormat
import java.util.*

interface FlightClickListener {
    fun onFlightClick(currency: String, flight: Flight)
}

class FlightsAdapter(private val flightClickListener: FlightClickListener) :
        ListAdapter<FlightListItem, FlightsViewHolder>(FlightsItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FlightsViewHolder(ListItemFlightBinding.inflate(layoutInflater, parent, false), flightClickListener)
    }

    override fun onBindViewHolder(holder: FlightsViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}

class FlightsViewHolder(private val viewBinding: ListItemFlightBinding,
                        private val flightClickListener: FlightClickListener): RecyclerView.ViewHolder(viewBinding.root) {

    fun bind(flightListItem: FlightListItem) {
        val remoteDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
        val timeDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

        flightListItem.flight.segments?.firstOrNull()?.time?.firstOrNull()?.let { remoteDepartureDateString ->
            val remoteDepartureDate = remoteDateFormat.parse(remoteDepartureDateString) ?: Date()
            viewBinding.departureTime.text = timeDateFormat.format(remoteDepartureDate)
        }

        flightListItem.flight.segments?.firstOrNull()?.time?.lastOrNull()?.let { remoteArrivalDateString ->
            val remoteArrivalDate = remoteDateFormat.parse(remoteArrivalDateString) ?: Date()
            viewBinding.arrivalTime.text = timeDateFormat.format(remoteArrivalDate)
        }

        flightListItem.flight.segments?.size?.let {
            val stops = if (it > 1) "$it stop" else "Direct"
            viewBinding.stops.text = stops
        }

        val duration = "Duration: ${flightListItem.flight.duration}"
        viewBinding.duration.text = duration

        viewBinding.departureStation.text = flightListItem.flight.segments?.firstOrNull()?.origin
        viewBinding.arrivalStation.text = flightListItem.flight.segments?.firstOrNull()?.destination


        val totalFare = flightListItem.flight.regularFare?.fares?.mapNotNull { it.amount?.times(it.count ?: 1) }?.sumOf { it }
        val price = "$totalFare\n${flightListItem.currency}"
        viewBinding.price.text = price

        viewBinding.root.setOnClickListener {
            flightClickListener.onFlightClick(flightListItem.currency ?: "", flightListItem.flight)
        }
    }
}

data class FlightListItem(val currency: String?, val flight: Flight)

class FlightsItemCallback : DiffUtil.ItemCallback<FlightListItem>() {
    override fun areItemsTheSame(oldItem: FlightListItem, newItem: FlightListItem) = true
    override fun areContentsTheSame(oldItem: FlightListItem, newItem: FlightListItem) = true
}