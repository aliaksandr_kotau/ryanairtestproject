package com.ryanair.testproject.presentation.flightdetails

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.ryanair.testproject.R
import com.ryanair.testproject.databinding.FragmentFlightDetailsBinding
import com.ryanair.testproject.domain.model.Flight
import dagger.hilt.android.AndroidEntryPoint

private const val CURRENCY_ARG = "currency_arg"
private const val FLIGHT_ARG = "flight_arg"

@AndroidEntryPoint
class FlightDetailsFragment : Fragment(R.layout.fragment_flight_details) {

    companion object {
        fun newInstance(currency: String,
                        flight: Flight) = FlightDetailsFragment().apply {
            arguments = Bundle().apply {
                putString(CURRENCY_ARG, currency)
                putParcelable(FLIGHT_ARG, flight)
            }
        }
    }

    private val flightDetailsViewModel by viewModels<FlightDetailsViewModel>()
    private val adapter = FlightDetailsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val flight = arguments?.getParcelable<Flight>(FLIGHT_ARG)

        val binding = FragmentFlightDetailsBinding.bind(view)

        (activity as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity?.title = flightDetailsViewModel.getTitle(flight)

        binding.flights.adapter = adapter
        flight?.let {
            adapter.submitList(it.segments ?: emptyList())
        }

        binding.price.text = flightDetailsViewModel.getPrice(arguments?.getString(CURRENCY_ARG), flight)
    }
}