package com.ryanair.testproject.presentation.flights

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ryanair.testproject.core.Resource
import com.ryanair.testproject.domain.model.Flight
import com.ryanair.testproject.domain.model.PassengersData
import com.ryanair.testproject.domain.model.Station
import com.ryanair.testproject.domain.usecase.GetFlightsListItemsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class FlightsViewModel @Inject constructor(private val getFlightsListItemsUseCase: GetFlightsListItemsUseCase) : ViewModel() {

    private val _title = MutableLiveData<String>()
    private val _data = MutableLiveData<List<FlightListItem>>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _errorMessage = MutableLiveData<String>()

    val title = _title as LiveData<String>
    val data = _data as LiveData<List<FlightListItem>>
    val isLoading = _isLoading as LiveData<Boolean>
    val errorMessage = _errorMessage as LiveData<String>

    init {
        _title.value = ""
    }

    fun getFlightsData(departureDateInMillis: Long?,
                       departureStation: Station?,
                       arrivalStation: Station?,
                       passengerData: PassengersData?) {
        val departureDate = departureDateInMillis?.let {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            dateFormat.format(Date(it))
        } ?: ""

        getFlightsListItemsUseCase(departureDate,
                departureStation?.code ?: "",
                arrivalStation?.code ?: "",
                passengerData?.adultCount.toString(),
                passengerData?.teenCount.toString(),
                passengerData?.childrenCount.toString(),
                passengerData?.infantCount.toString()).onEach { resource ->
            when(resource) {
                is Resource.Loading -> {
                    _isLoading.value = true
                }
                is Resource.Success -> {
                    _isLoading.value = false

                    resource.data?.let {
                        _title.value = getTitle(it.first().flight)
                        _data.value = it
                    }
                }
                is Resource.Error -> {
                    _isLoading.value = false
                    resource.errorMessage?.let {
                        _errorMessage.value = it
                    }
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun getTitle(flight: Flight?) =
        "${flight?.segments?.firstOrNull()?.origin ?: ""} -> ${flight?.segments?.lastOrNull()?.destination ?: ""}"
}