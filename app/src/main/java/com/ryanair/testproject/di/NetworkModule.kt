package com.ryanair.testproject.di

import com.ryanair.testproject.core.BASE_URL_FLIGHTS
import com.ryanair.testproject.core.BASE_URL_STATIONS
import com.ryanair.testproject.data.remote.FlightsApi
import com.ryanair.testproject.data.remote.StationsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class StationsRetrofitInstance

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class FlightsRetrofitInstance

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @StationsRetrofitInstance
    @Provides
    @Singleton
    fun provideStationsRetrofit(): Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL_STATIONS)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @FlightsRetrofitInstance
    @Provides
    @Singleton
    fun provideFlightsRetrofit(): Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL_FLIGHTS)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideStationsApi(@StationsRetrofitInstance retrofit: Retrofit): StationsApi =
        retrofit.create(StationsApi::class.java)

    @Provides
    @Singleton
    fun provideFlightsApi(@FlightsRetrofitInstance retrofit: Retrofit): FlightsApi =
        retrofit.create(FlightsApi::class.java)
}