package com.ryanair.testproject.di

import com.ryanair.testproject.data.remote.FlightsApi
import com.ryanair.testproject.data.remote.StationsApi
import com.ryanair.testproject.data.repository.RyanairRepositoryImpl
import com.ryanair.testproject.domain.repository.RyanairRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RyanairRepositoryModule {

    @Provides
    @Singleton
    fun provideRyanairRepository(stationsApi: StationsApi,
                                 flightsApi: FlightsApi): RyanairRepository =
        RyanairRepositoryImpl(stationsApi, flightsApi)
}