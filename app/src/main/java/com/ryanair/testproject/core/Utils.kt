package com.ryanair.testproject.core

import android.content.Context
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class Utils @Inject constructor(@ApplicationContext private val applicationContext: Context) {

    fun getString(@StringRes resId: Int) = applicationContext.getString(resId)
    fun getQuantityString(@PluralsRes stringId: Int, count: Int, vararg formatParams: Any): String =
        applicationContext.resources.getQuantityString(stringId, count, *formatParams)
}