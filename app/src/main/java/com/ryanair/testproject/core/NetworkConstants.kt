package com.ryanair.testproject.core

internal const val BASE_URL_STATIONS = "https://mobile-testassets-dev.s3.eu-west-1.amazonaws.com/"
internal const val BASE_URL_FLIGHTS = "https://www.ryanair.com/api/booking/v4/en-gb/"

internal const val STATIONS_PATH = "stations.json"
internal const val FLIGHTS_PATH = "Availability?ToUs=AGREED"
